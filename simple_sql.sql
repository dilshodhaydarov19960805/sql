-- posrgresql isga tushgan vaqtni olish
select date_trunc('second', current_timestamp - pg_postmaster_start_time()) as d;

-- mavjud scram encryption ko'rish
show password_encryption;


-- ########################### EXTENSION ##################################
-- INSTALL plpgsql extension
create extension plpgsql;
-- ########################### EXTENSION ##################################

-- ########################### FUNCTION ##################################
-- functionlar ro'yxatni ko'rish
SELECT proname, prosrc
FROM pg_proc
WHERE proname like 'log_%';

-- mavjud function ni o'chirib yuborish
drop function if exists get_user_age(alumentId INT);

--  function yaratish misol
CREATE FUNCTION get_user_age(alimentId INT) RETURNS text AS
$$
DECLARE
    passport_series_number text;
BEGIN
    SELECT aliment.passport_series_number
    INTO passport_series_number
    FROM aliment
    WHERE id = alimentId;

    RETURN passport_series_number;
END;
$$ LANGUAGE plpgsql;

-- function ni ishlatish
select get_user_age(1);

-- ########################### FUNCTION ##################################

-- ########################### TRIGGER ##################################
-- mavjud triggerlarni ko'rish
SELECT trigger_name, event_object_table, action_statement
FROM information_schema.triggers
WHERE trigger_schema = 'public';

-- TRIGGER YARATISH VA MISOLDA KO'RISH
DROP TRIGGER IF EXISTS aliment_insert_or_update_or_trigger on aliment;
DROP TRIGGER IF EXISTS aliment_delete_trigger on aliment;
drop function if exists log_aliment_insert_update_delete();


CREATE TABLE if not exists audit_log
(
    id          SERIAL PRIMARY KEY,
    action_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    action_type TEXT,
    aliment_id  INT,
    pin         text
);

CREATE OR REPLACE FUNCTION log_aliment_insert_update_delete() RETURNS TRIGGER AS
$$
BEGIN
    INSERT INTO audit_log (action_type, aliment_id, pin)
    VALUES (CASE
                WHEN TG_OP = 'INSERT' THEN 'INSERT'
                WHEN TG_OP = 'UPDATE' THEN 'UPDATE'
                WHEN TG_OP = 'DELETE' THEN 'DELETE'
                END,
            COALESCE(NEW.id, OLD.id),
            COALESCE(NEW.pin, OLD.pin));
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER aliment_insert_or_update_or_trigger
    AFTER INSERT OR UPDATE
    ON aliment
    FOR EACH ROW
EXECUTE FUNCTION log_aliment_insert_update_delete();

CREATE TRIGGER aliment_delete_trigger
    BEFORE DELETE
    ON aliment
    FOR EACH ROW
EXECUTE FUNCTION log_aliment_insert_update_delete();

-- ########################### TRIGGER ##################################

-- ########################### PROCEDURE ##################################
-- ########################### UPDATE ##################################
CREATE PROCEDURE update_aliment_pin(aliment_id INT, new_pin text)
    LANGUAGE plpgsql AS
$$
BEGIN
    UPDATE aliment SET pin = new_pin WHERE id = aliment_id;
END;
$$;

CALL update_aliment_pin(5, '30508965637697');

-- ########################### DELETE ##################################

CREATE PROCEDURE delete_aliment_pin(aliment_id INT)
    LANGUAGE plpgsql AS
$$
BEGIN
    delete from aliment WHERE id = aliment_id;
END;
$$;

CALL delete_aliment_pin(4);
-- ########################### PROCEDURE ##################################
DELETE
FROM public.aliment
WHERE id = 4;

-- bu sql ni boshqa table dan ma'lumotlari bilan birga nusxa olish uchun foydalanish mumkin
create table if not exists test as
select *
from aliment;

-- bu turdagi table vaqtinchalik ishlatiladigan table bo'lib boshqa table ga nisbatanancha tez bo'ladi.
-- bu table xotrada saqlaydi agar system o'chib yonsa table o'chib ketadi.
create unlogged table if not exists test_unlogged as
select *
from aliment;

-- ###################################### VIEW #########################################
-- Dynamic rafishda yangilanadi.
-- Bu ma'lumotni har safar asosiy table dan oladi
-- Bu DBMS lar view ni malumotni qisman cache laydi
-- Bu sql ni soddalashtrishga va murrakkab sqlni saqlab qo'yishga imkon beradi
-- Malumotni saqlamaydi view faqat sql ni saqalydi
-- Xafzizlikni oshirish mumkin va kerakli datani ko'rsatadigan qilib nastroyka qilish mumkin.

-- ###################################### Kamchiliklari #########################################
-- Performance ko'p talab qilish holallari bor ko'p zapros kelsa katta performance talab qiladi
drop view aliment_view;

CREATE VIEW aliment_view AS
SELECT id, pin, passport_series_number
FROM aliment
WHERE id > 4;
-- ###################################### VIEW #########################################

--  #########################################  MATERIALIZED VIEW  #########################################
drop materialized view aliment_mv;

CREATE MATERIALIZED VIEW aliment_mv AS
SELECT pin, id, passport_series_number
FROM aliment
WHERE aliment.id > 4;
-- Performance tarafdan yaxshi murrakkab zqproslarda ham tez va yaxshi va performance ko'p talab etilmaydi
-- Murakkab sql larni saqlab qo'yish imkoni beradi
-- Ma'lumotni disk da table kabi saqlaydi
-- xafsizlik uchun ham nastroyka qilish mumkin role lar asosida
--  static malumot saqlanadi

-- ###################################### Kamchiliklari #########################################

-- Ma'lumotlar diskda saqlanganligi sababli malumot automat yangilanmaydi.
-- Ma'lumotlarni yangilash uchun bir nechta usuldan foydalanish orqali automat yangilaydigan qilish mumkin
-- 1. har safar qolda sql orqali

REFRESH MATERIALIZED VIEW aliment_mv;

-- 2. cron job orqali
CREATE EXTENSION pg_cron;
SELECT cron.schedule('0 * * * *', 'REFRESH MATERIALIZED VIEW employee_salaries');
-- 3. Trigger orqali

drop function refresh_aliment_mv_fn();
-- 3.1 trigger function
CREATE OR REPLACE FUNCTION refresh_aliment_mv_fn()
    RETURNS TRIGGER AS
$$
BEGIN
    REFRESH MATERIALIZED VIEW aliment_mv;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

drop trigger if exists refresh_aliment_mv_tr on aliment;
-- 3.2 creation trigger
CREATE TRIGGER refresh_aliment_mv_tr
    AFTER INSERT OR UPDATE OR DELETE
    ON aliment
    FOR EACH STATEMENT
EXECUTE FUNCTION refresh_aliment_mv_fn();

-- 4. javada schedule orqali
REFRESH MATERIALIZED VIEW aliment_mv;
-- dan foydalangan holda

-- ###################################### VIEW #########################################

-- ###################################### VIEW and MATERIALIZED VIEW difference #########################################
-- Performance talab qilish dagi farqlar
-- Malumotlarni saqlash dagi farqlar
-- Ma'lumotlarni yangilashdagi farqlar
select *
from aliment_mv;